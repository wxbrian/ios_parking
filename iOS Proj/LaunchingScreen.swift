//
//  LaunchingScreen.swift
//  iOS Proj
//
//  Created by Abita Shiney on 2/23/18.
//  Copyright © 2018 Abita Shiney. All rights reserved.
//

import UIKit

class LaunchingScreen: UIViewController {

    @IBOutlet weak var ProgressBar: UIProgressView!
    
    var timer = Timer()
    var seconds = 10
    override func viewDidLoad() {
        super.viewDidLoad()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector (showProgress), userInfo: nil, repeats: true)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
