//
//  RegisterViewController.swift
//  iOS Proj
//
//  Created by Abita Shiney on 2/23/18.
//  Copyright © 2018 Abita Shiney. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet weak var txtName: UITextField!
    
    @IBOutlet weak var txtEmail: UITextField!
    
    
    @IBOutlet weak var txtPassword: UITextField!
    
    
    @IBOutlet weak var txtContNum: UITextField!

    
    @IBOutlet weak var txtPlatNum: UITextField!
    
    

    @IBOutlet weak var btnReg: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtName.layer.borderWidth = 1
        txtName.layer.borderColor = UIColor.white.cgColor
        txtName.layer.cornerRadius = 10
        txtName.clipsToBounds = true
        
        txtEmail.layer.borderWidth = 1
        txtEmail.layer.borderColor = UIColor.white.cgColor
        txtEmail.layer.cornerRadius = 10
        txtEmail.clipsToBounds = true

        txtPassword.layer.borderWidth = 1
        txtPassword.layer.borderColor = UIColor.white.cgColor
        txtPassword.layer.cornerRadius = 10
        txtPassword.clipsToBounds = true

        txtContNum.layer.borderWidth = 1
        txtContNum.layer.borderColor = UIColor.white.cgColor
        txtContNum.layer.cornerRadius = 10
        txtContNum.clipsToBounds = true

        txtPlatNum.layer.borderWidth = 1
        txtPlatNum.layer.borderColor = UIColor.white.cgColor
        txtPlatNum.layer.cornerRadius = 10
        txtPlatNum.clipsToBounds = true
        
        btnReg.layer.cornerRadius = 15
        btnReg.clipsToBounds = true

       
    }
/*
    
    @IBAction func btnRegister(_ sender: UIButton) {
        var cust = Customer()
        cust.custName = txtName
        cust.custEmail = txtEmail
        cust.custPassword = txtPassword
        cust.custContNum = txtContNum
        cust.custPlatNum = txtPlatNum
        
        let flag = Customer.addCustomer(cust: cust)
        if flag == true{
            print("Employee Record Saved")
        }else{
            print("Possible duplication error!!")
        }
    }
 */
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    

   

}
